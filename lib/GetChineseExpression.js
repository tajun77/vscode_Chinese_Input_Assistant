var 五笔 = require('./WuBi.js')
var 拼音A = require('./PinYin.js')
var 拼音 = require('pinyin')
var { 多重笛卡尔积 } = require('./ArrayEx.js')
var { 转换为大写 } = require('./StringEx')

module.exports = function 获得中文字符表示({ 表示方法, 文本, 选项 }) {
    if (表示方法.indexOf("五笔") != -1) {
        return [五笔.五笔(文本, 表示方法)]
    }
    if (表示方法.indexOf('拼音')) {
        var 结果 = null
        结果 = 拼音(文本.replace(/〇/g, '零'), { heteronym: true, style: 拼音.STYLE_NORMAL })
        结果 = 结果.map(a => a.map(a => 转换为大写(a[0]) + a.substring(1)))
        结果 = 多重笛卡尔积(结果).map(a => a.join(''))
        if (表示方法 != "全拼")
            结果 = 结果.map(a => 拼音A.双拼转换(a, 表示方法))
        return 结果
    }
}
